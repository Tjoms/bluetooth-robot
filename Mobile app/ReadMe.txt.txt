How to download the app:

1. Download the app to your computer

2. Connect your phone to the computer via USB

3. From your computer: Go to the download folder on your phone.

4. From your computer: Copy the app and paste it to the download folder on your phone.

5. From your phone: Go to the download folder where the app was pasted.

6. From your phone: Click on the app to install it (It may ask you to change some settings for it to install)


Feel free to use any folder on your phone, you dont have to use the download folder.   