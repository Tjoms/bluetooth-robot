/*
 * File:   Bluetooth.c
 * Author: boi
 *
 * Created on 08 June 2020, 12:31
 */


#include "allcode_api.h"
#include "PID.h"
#include "IR_detector.h"

/* Controller parameters */
#define PID_KP  0.5f
#define PID_KI  1.5f //0.75
#define PID_KD  0.00f //0.005

#define PID_TAU 0.01f

float PID_LIM_MIN = -100.0f;
float PID_LIM_MAX = 100.0f;

#define SAMPLE_TIME_S 0.1f
#define SAMPLE_TIME_MS 100.0f

float setpointLeft = 0.0f;
float setpointRight = 0.0f;
float integrator = 0.0f;

bool isPIDActive = true;

/* IR detector parameters*/
int rangeMax = 50;
bool IR_Active[8] = {false, false, false, false, false, false, false, false};

/* User input default*/
int input = -2;

/* Speed control parameters*/
int signLeft = -1;
int signRight = -1;
int speedLeft = 0;
int speedRight = 0;

//Removes "clogged" data at the beginning of the program

void clearBluetooth() {
    int i = 0;
    int tempInput = 0;
    for (i = 0; i < 255; i++) {
        tempInput = FA_BTGetByte();
        if (tempInput == -1) {
            break;
        }
    }
}

//double readIR(int sensor) {
//    double IRAverage = 0;
//    int currentSample = 0;
//    int totalSample = 50;
//    //Takes an average to eliminate noise
//    while (currentSample < totalSample) {
//        int IRValue = FA_ReadIR(sensor); //0 left, 1 left-front, 2 front, 3 right-front .....
//
//        IRAverage += (double) IRValue;
//        currentSample++;
//    }
//    IRAverage /= currentSample;
//
//    return IRAverage;
//}

//Prints information about the system. Printing everything takes more than the given sampletime *issue*

void printInfo(int pidLeftOut, int pidRightOut, int speedLeft, int speedRight, int measurementLeft, int measurementRight, int tempInput) {
    FA_LCDClear();
    //            FA_LCDPrint("Output:", 7, 0, 0, FONT_NORMAL, LCD_OPAQUE);
    //            FA_LCDPrint("Setpoint:", 9, 0, 8, FONT_NORMAL, LCD_OPAQUE);
    //            FA_LCDPrint("Measurement:", 12, 0, 16, FONT_NORMAL, LCD_OPAQUE);
    //
    //            FA_LCDNumber(pidLeft.out, 110, 0, FONT_NORMAL, LCD_OPAQUE);
    //            FA_LCDNumber(speedLeft, 110, 8, FONT_NORMAL, LCD_OPAQUE);
    FA_LCDNumber(measurementLeft, 110, 16, FONT_NORMAL, LCD_OPAQUE);
    //
    //            FA_LCDNumber(pidRight.out, 80, 0, FONT_NORMAL, LCD_OPAQUE);
    //            FA_LCDNumber(speedRight, 80, 8, FONT_NORMAL, LCD_OPAQUE);
    FA_LCDNumber(measurementRight, 80, 16, FONT_NORMAL, LCD_OPAQUE);
    //
    FA_LCDNumber(input, 80, 24, FONT_NORMAL, LCD_OPAQUE);

}

int main(void) {
    FA_RobotInit();

    /* Initialise PID controller */
    PIDController pidLeft = {PID_KP, PID_KI, -PID_KD,
        PID_TAU,
        PID_LIM_MIN, PID_LIM_MAX,
        SAMPLE_TIME_S};

    PIDController pidRight = {PID_KP, PID_KI, -PID_KD,
        PID_TAU,
        PID_LIM_MIN, PID_LIM_MAX,
        SAMPLE_TIME_S};

    PIDController_Init(&pidLeft);
    PIDController_Init(&pidRight);

    /* Initialise IR detectors*/
    Detector detector = {rangeMax};
    Detector_Init(&detector, IR_Active);

    clearBluetooth();
    FA_LCDBacklight(50);
    FA_DelayMillis(1000);

    unsigned long prevTime = FA_ClockMS();
    while (1) {
        Detector_UpdateActiveDetectors(&detector, IR_Active);

        int tempInput = FA_BTGetByte();
        //Update input if data is recived
        if (tempInput != -1) input = tempInput;


        if (FA_BTConnected() == 1) {
            FA_LCDPrint("Connected:", 10, 0, 0, FONT_NORMAL, LCD_OPAQUE);
        } else if (FA_BTConnected() == 0) {
            FA_LCDPrint("Not connected:", 14, 0, 0, FONT_NORMAL, LCD_OPAQUE);
            input = 0;
        }

        switch (input) {
            case 255: //Left
                PIDController_Init(&pidLeft);
                PIDController_Init(&pidRight);

                signLeft = -1;
                signRight = 1;
                break;
            case 254: //Right
                PIDController_Init(&pidLeft);
                PIDController_Init(&pidRight);

                signLeft = 1;
                signRight = -1;
                break;
            case 253: //Forward
                PIDController_Init(&pidLeft);
                PIDController_Init(&pidRight);

                signLeft = 1;
                signRight = 1;
                break;
            case 252: //Backward
                PIDController_Init(&pidLeft);
                PIDController_Init(&pidRight);

                signLeft = -1;
                signRight = -1;
                break;
            case 251:
                IR_Active[0] = true;
                break;
            case 250:
                IR_Active[0] = false;
                break;
            case 249:
                IR_Active[1] = true;
                break;
            case 248:
                IR_Active[1] = false;
                break;
            case 247:
                IR_Active[2] = true;
                break;
            case 246:
                IR_Active[2] = false;
                break;
            case 245:
                IR_Active[3] = true;
                break;
            case 244:
                IR_Active[3] = false;
                break;
            case 243:
                IR_Active[4] = true;
                break;
            case 242:
                IR_Active[4] = false;
                break;
            case 241:
                IR_Active[5] = true;
                break;
            case 240:
                IR_Active[5] = false;
                break;
            case 239:
                IR_Active[6] = true;
                break;
            case 238:
                IR_Active[6] = false;
                break;
            case 237:
                IR_Active[7] = true;
                break;
            case 236:
                IR_Active[7] = false;
                break;
            case 235:
                isPIDActive = true;
                break;
            case 234:
                isPIDActive = false;
                break;
            default:
                //From 0-130 encoder ticks (setpoint)
                setpointLeft = input;
                setpointRight = input;
                break;
        }
        //Stop if there is an object in the way of the robot
        if (Detector_RangeReached(&detector)) {
            signLeft = 0;
            signRight = 0;
        }

        if (isPIDActive) {
            //Update PID every SAMPLE_TIME_MS
            if ((prevTime + SAMPLE_TIME_MS <= FA_ClockMS())) {
                float measurementLeft = FA_ReadEncoder(0);
                float measurementRight = FA_ReadEncoder(1);

                PIDController_Update(&pidLeft, setpointLeft, measurementLeft);
                PIDController_Update(&pidRight, setpointRight, measurementRight);

                //Integrates the left and right motor
                integrator = (pidLeft.out - pidRight.out) / 2;
                //Sets motor speed with signLeft/signRight indicating direction
                FA_SetMotors((pidLeft.out + integrator) * signLeft, (pidRight.out - integrator) * signRight);

                //Reset
                FA_ResetEncoders();
                prevTime = FA_ClockMS();

                printInfo(pidLeft.out, pidRight.out, speedLeft, speedRight, measurementLeft, measurementRight, tempInput);
            }
        } else {
            FA_SetMotors(setpointLeft * signLeft, setpointRight * signRight);
        }
    }
    return 0;
}
