/*
 * File:   IR_Detector.c
 * Author: boi
 *
 * Created on 07 September 2020, 17:09
 */


#include "IR_Detector.h"
#include "allcode_api.h"

void Detector_Init(Detector *detector, bool IR_Active[]) {
    int IR = 0;
    for (; IR < 8; IR++) {
        detector->IR_Active[IR] = IR_Active[IR];
        detector->IR_Values[IR] = 0;
    }
}

bool Detector_RangeReached(Detector *detector) {
    bool rangeReached = false;
    int IR = 0;
    for (; IR < 8; IR++) {
        if (detector->rangeMax < detector->IR_Values[IR]) {
            rangeReached = true;
            break;
        }
    }
    return rangeReached;
}
//Display Active IR with LEDS on the robot. IR_0 == LED_0, IR_1 == LED_1, ...

void Detector_ShowActiveDetectors(Detector *detector) {
    int IR = 0;
    for (; IR < 8; IR++) {
        if (detector->IR_Active[IR] == 1) FA_LEDOn(IR);
        if (detector->IR_Active[IR] == 0) FA_LEDOff(IR);
    }
}
//Updates all the active detectors and sets a 0 to all inactive detectors
void Detector_UpdateActiveDetectors(Detector * detector, bool IR_Active[]) {
    int IR = 0;
    for (; IR < 8; IR++) {
        detector->IR_Active[IR] = IR_Active[IR];
        if (detector->IR_Active[IR] == 1) {
            detector->IR_Values[IR] = Detector_UpdateIRValue(IR);
        } else {
            detector->IR_Values[IR] = 0;
        }
    }
    Detector_ShowActiveDetectors(detector);

}

// Returns an average of multiple values from the same IR for a more accurate reading
double Detector_UpdateIRValue(int IR) {
    double IRAverage = 0;
    int currentSample = 0;
    int totalSample = 6;
    //Takes an average to eliminate noise
    while (currentSample < totalSample) {
        int IRValue = FA_ReadIR(IR); //0 left, 1 left-front, 2 front, 3 right-front .....

        IRAverage += (double) IRValue;
        currentSample++;
    }
    IRAverage /= currentSample;


    return IRAverage;
}
