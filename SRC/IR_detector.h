#ifndef IR_DETECTOR_H
#define IR_DETECT0R_H

#include "allcode_api.h"


typedef struct {
    int rangeMax;

    bool IR_Active[8];
    int IR_Values[8];

} Detector;

void Detector_Init(Detector *detector, bool IR_Active[]);
bool Detector_rangeReached(Detector *detector);
void Detector_ShowActiveDetectors(Detector *detector);
void Detector_UpdateActiveDetectors(Detector *detector, bool IR_Active[]);
void Detector_UpdateIRValues(Detector *detector);
double Detector_UpdateIRValue(int IR);
#endif